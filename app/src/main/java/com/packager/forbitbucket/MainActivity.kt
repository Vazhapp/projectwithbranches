package com.packager.forbitbucket

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.packager.forbitbucket.ui.theme.ForBitbucketTheme
import com.packager.forbitbucket.ui.theme.MainText

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ForBitbucketTheme {
                Greeting(name = "Master branch")
            }
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "$name!",
        color = MainText,
        fontSize = 30.sp,
        fontStyle = FontStyle.Italic,
        textAlign = TextAlign.Center,
        modifier = Modifier.padding(150.dp, 150.dp)
    )
}